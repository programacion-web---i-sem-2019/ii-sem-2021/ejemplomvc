/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author madar
 */
public class Estudiante implements Comparable {

    private int codigo;
    private String nombre;
    private float previo1, previo2, previo3, examen;

    public Estudiante() {
    }

    public Estudiante(int codigo, String nombre, float previo1, float previo2, float previo3, float examen) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.previo1 = previo1;
        this.previo2 = previo2;
        this.previo3 = previo3;
        this.examen = examen;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrevio1() {
        return previo1;
    }

    public void setPrevio1(float previo1) {
        this.previo1 = previo1;
    }

    public float getPrevio2() {
        return previo2;
    }

    public void setPrevio2(float previo2) {
        this.previo2 = previo2;
    }

    public float getPrevio3() {
        return previo3;
    }

    public void setPrevio3(float previo3) {
        this.previo3 = previo3;
    }

    public float getExamen() {
        return examen;
    }

    public void setExamen(float examen) {
        this.examen = examen;
    }

    @Override
    public int compareTo(Object o) {
        Estudiante x = (Estudiante) o;
        return this.codigo - x.codigo;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", nombre=" + nombre + ", previo1=" + previo1 + ", previo2=" + previo2 + ", previo3=" + previo3 + ", examen=" + examen + '}';
    }

    
    
}
