/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import DTO.Estudiante;
import java.util.TreeSet;

/**
 *
 * @author madar
 */
public class SIA {

    TreeSet<Estudiante> estudiantes = new TreeSet();

    public SIA() {
    }

    public Estudiante crearEstudiante(int cod, String nombre, float p1, float p2, float p3, float exm) {

        return new Estudiante(cod, nombre, p1, p2, p3, exm);

    }

    public boolean inserEstudiante(int cod, String nombre, float p1, float p2, float p3, float exm) {
        Estudiante nuevo = this.crearEstudiante(cod, nombre, p1, p2, p3, exm);
        return this.estudiantes.add(nuevo);

    }

    public String getListadoEstudiantes() {
        String msg = "";
        for (Estudiante x : this.estudiantes) {
            msg += "<br>" + x.toString();
        }
        return msg;
    }
}
