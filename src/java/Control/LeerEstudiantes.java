/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.Estudiante;
import Negocio.SIA;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author madar
 */
public class LeerEstudiantes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet LeerEstudiantes</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet LeerEstudiantes at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }

        try {
            String codigo = request.getParameter("codigo");
            String nombre = request.getParameter("nombre");
            String p1 = request.getParameter("p1");
            String p2 = request.getParameter("p2");
            String p3 = request.getParameter("p3");
            String examen = request.getParameter("examen");
            //Conversión de datos
            int pcodigo = Integer.parseInt(codigo);
            float pp1 = Float.parseFloat(p1);
            float pp2 = Float.parseFloat(p2);
            float pp3 = Float.parseFloat(p3);
            float exa = Float.parseFloat(examen);

            validar(pp1,pp2,pp3,exa);
            
            SIA sia = new SIA();

            if (request.getSession().getAttribute("sia") != null) {
                sia = (SIA) request.getSession().getAttribute("sia");
            }

            if (sia.inserEstudiante(pcodigo, nombre, pp1, pp2, pp3, exa)) {
                request.getSession().setAttribute("sia", sia);
                request.getRequestDispatcher("./jsp/listarEstudiantes.jsp").forward(request, response);
            } else {
                request.getSession().setAttribute("error", "Estudiante ya existe");
                request.getRequestDispatcher("./error/error.jsp").forward(request, response);

            }

        } catch (Exception e) {
            request.getSession().setAttribute("error", e.getMessage());
            request.getRequestDispatcher("./error/error.jsp").forward(request, response);
        }

//        Estudiante nuevo = sia.crearEstudiante(pcodigo, nombre, pp1, pp2, pp3, exa);
//        request.setAttribute("respuesta", nuevo); //Object
//        request.getRequestDispatcher("./jsp/crear.jsp").forward(request, response);
    }

    private void validar(float... previos) {
        for(float x:previos)
        {
            if(x<0 || x>5)
                throw new RuntimeException("El valor está fuera de rango");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
