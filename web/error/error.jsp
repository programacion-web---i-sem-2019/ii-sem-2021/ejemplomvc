<%-- 
    Document   : error
    Created on : 9/12/2021, 09:24:12 AM
    Author     : madar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
        <h1>Ha ocurrido un error!</h1>
        
        <%
            Object error=request.getSession().getAttribute("error");
        %>
        
        <%= error.toString() %>
        
    </body>
</html>
