<%-- 
    Document   : crear
    Created on : 9/12/2021, 08:02:00 AM
    Author     : madar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ejemplo de lectura de datos</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <h1>Leyendo datos de un Estudiante</h1>


        <%
            String mensaje = "Esto es un ejemplo";
            Object objeto = request.getAttribute("respuesta");
            DTO.Estudiante est = (DTO.Estudiante) objeto;

        %>


        <h1>

            <%=mensaje%>

        </h1>

        <h2>
            <p>
                Los datos del estudiante son:
                <br><%=est.getNombre()%>
                <br><%=est.getCodigo()%>
                <br><%=est.getPrevio1()%>
                <br><%=est.getPrevio2()%>
                <br><%=est.getPrevio3()%>
                <br><%=est.getExamen()%>

            </p>

        </h2>





    </body>
</html>
