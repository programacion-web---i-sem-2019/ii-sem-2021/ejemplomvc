<%-- 
    Document   : listarEstudiantes
    Created on : 9/12/2021, 09:15:52 AM
    Author     : madar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listado de estudiantes</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <h1>Estudiantes registrados:</h1>
        
        <%
        
            Negocio.SIA sia=(Negocio.SIA)request.getSession().getAttribute("sia");
            //permitir mantener el objeto en memoria
            request.getSession().setAttribute("sia", sia);
            
        %>
        
        <%=sia.getListadoEstudiantes()%>
    </body>
</html>
